const http = uni.$u.http

// post请求，获取菜单
export const postMenu = (params, config = {}) => http.post('/ebapi/public_api/index', params, config)

// get请求，获取菜单，注意：get请求的配置等，都在第二个参数中，详见前面解释
export const getMenu = (data) => http.get('/ebapi/public_api/index', data)
//会员登录 /api/user/login
export const login = (params, config = {}) => http.post('/api/Index/userApply', params, config)
///api/Index/scanInfo
export const scanInfo = (params, config = {}) => http.post('/api/Index/scanInfo', params, config)
///api/Index/moneyList
export const moneyList = (params, config = {}) => http.post('/api/Index/moneyList', params, config)
///api/Index/login
export const loginSend = (params, config = {}) => http.post('/api/Index/login', params, config)
///api/Sms/send
export const smsSend = (params, config = {}) => http.post('/api/Sms/send', params, config)